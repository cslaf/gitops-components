## 0.1.15 (2024-04-25)

### Fix

- rename target_repo

## 0.1.14 (2024-04-25)

### Fix

- update downstream infrastructure url and target_repo

## 0.1.13 (2024-04-25)

### Fix

- ignore triggered job

## 0.1.12 (2024-04-25)

### Fix

- jq syntax

## 0.1.11 (2024-04-25)

### Fix

- split out job added script for debugging

## 0.1.10 (2024-04-24)

### Fix

- update deploy

## 0.1.9 (2024-04-24)

### Fix

- bash syntax again

## 0.1.8 (2024-04-24)

### Fix

- cleanup expected job outputs

## 0.1.7 (2024-04-24)

### Fix

- update bash syntax

## 0.1.6 (2024-04-24)

### Fix

- update conditional for ensure-job-added

## 0.1.5 (2024-04-24)

### Fix

- standardize input naming conventions feat: add test, licence and infrastructure_repo input

## 0.1.4 (2024-04-23)

### Fix

- build off tags by default to skip duplicate image builds

## 0.1.3 (2024-04-23)

### Fix

- stop duplicate builds and don't fail bump on no version change

## 0.1.2 (2024-04-23)

### Fix

- make build/bump/deploy interruptable

## 0.1.1 (2024-04-23)

### Fix

- add Containerfile for full testing and update commit tag regex

## 0.1.0 (2024-04-23)

### Feat

- add build/bump/deploy/release templates

### Fix

- update image-name reference
